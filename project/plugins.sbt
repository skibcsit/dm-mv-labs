addSbtPlugin("org.scala-js" % "sbt-scalajs" % "1.1.1")
addSbtPlugin("ch.epfl.scala" % "sbt-scalajs-bundler" % "0.18.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.8.0")
addSbtPlugin("com.heroku" % "sbt-heroku" % "2.1.4")
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.0")

